import functools
import logging
from dataclasses import dataclass
from typing import Any, Dict, List, Optional

import aiohttp  # type: ignore

from openweathermap import exceptions, models


@dataclass
class OpenWeatherBase:
    appid: str
    base_url = ""

    def _url_formatter(self, url: str) -> str:
        url = url[1:] if url.startswith("/") else url
        return f"{self.base_url}/{url}"

    @staticmethod
    async def _read_response(resp, action: str):
        if resp.status == 200:
            method = getattr(resp, action)
            result = await method()
        else:
            message = f"{resp.url} returned {resp.status}"
            logging.error(message)
            raise exceptions.BadRequest(resp.status)
        return result

    async def _json_request(self, url: str, params: Optional[Dict[str, Any]] = None) -> Any:
        if params is None:
            params = {}
        url = self._url_formatter(url)
        async with aiohttp.ClientSession() as session:
            async with (session.get(url=url, params=params)) as resp:
                result = await self._read_response(resp, "json")
        return result

    async def _binary_request(self, url: str, params: Optional[Dict[str, Any]] = None) -> bytes:
        if params is None:
            params = {}
        url = self._url_formatter(url=url)
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params) as resp:
                result = await self._read_response(resp, "read")
        return result

    def __hash__(self) -> int:
        return hash((self.appid, self.base_url))


@dataclass
class OpenWeatherData(OpenWeatherBase):
    """
    Documentation at: https://openweathermap.org/api/one-call-api
    """

    base_url = "https://api.openweathermap.org/data/2.5"

    async def _basic_request(self, url: str, lat: float, lon: float, **kwargs) -> Any:
        params = {
            "lat": lat,
            "lon": lon,
            "appid": self.appid,
        }  # type: Dict[str, Any]
        params.update(**kwargs)
        return await self._json_request(url=url, params=params)

    async def one_call(self, lat: float, lon: float, units: str) -> models.OneCallAPIResponse:
        result = await self._basic_request(url="/onecall", lat=lat, lon=lon, units=units)
        return models.OneCallAPIResponse(**result)

    async def air_pollution(self, lat: float, lon: float) -> models.AirPollutionAPIResponse:
        result = await self._basic_request(url="/air_pollution", lat=lat, lon=lon)
        return models.AirPollutionAPIResponse(**result)

    async def air_pollution_forecast(self, lat: float, lon: float) -> models.AirPollutionAPIResponse:
        result = await self._basic_request(url="/air_pollution/forecast", lat=lat, lon=lon)
        return models.AirPollutionAPIResponse(**result)

    async def air_pollution_history(
        self, lon: float, lat: float, start: int, end: int
    ) -> models.AirPollutionAPIResponse:
        result = await self._basic_request(
            url="/air_pollution/history",
            lat=lat,
            lon=lon,
            start=start,
            end=end,
        )
        return models.AirPollutionAPIResponse(**result)

    async def uvi(self, lat: float, lon: float) -> models.UviAPIResponse:
        result = await self._basic_request(url="/uvi", lat=lat, lon=lon)
        return models.UviAPIResponse(**result)

    async def uvi_forecast(self, lat: float, lon: float, cnt: int) -> List[models.UviAPIResponse]:
        result = await self._basic_request(url="/uvi/forecast", lat=lat, lon=lon, cnt=cnt)
        return [models.UviAPIResponse(**data) for data in result]

    async def uvi_history(self, lat: float, lon: float, cnt: int, start: int, end: int) -> models.UviListAPIResponse:
        result = await self._basic_request(
            url="/uvi/history",
            lat=lat,
            lon=lon,
            cnt=cnt,
            start=start,
            end=end,
        )
        return models.UviListAPIResponse(**result)


@dataclass
class OpenWeatherMap(OpenWeatherBase):
    """
    Documentation at: https://openweathermap.org/api/weathermaps
    """

    base_url = "https://tile.openweathermap.org/map"

    async def _basic_request(self, layer: str, x: int, y: int, z: int) -> bytes:
        url = f"/{layer}/{z}/{x}/{y}.png"
        return await self._binary_request(url=url, params={"appid": self.appid})

    def __getattribute__(self, attr) -> Any:
        # enables map endpoints to accessed without repetitive code
        # for instance: map = self.clouds(x,y,z)
        if attr in [
            "clouds",
            "precipitation",
            "pressure",
            "wind",
            "temp",
        ]:
            # returns a callable coro
            return functools.partial(self._basic_request, f"{attr}_new")
        return super().__getattribute__(attr)


class OpenWeatherGeocoding(OpenWeatherBase):

    base_url = "https://api.openweathermap.org/geo/1.0"

    async def geocode(
        self, city: str, state: str, country: str, limit: int = None
    ) -> List[models.GeocodingAPIResponse]:
        params: Dict[str, Any] = {
            "appid": self.appid,
            "q": f"{city},{state},{country}",
        }
        if limit:
            params.update({"limit": limit})
        result = await self._json_request(url="/direct", params=params)
        return [models.GeocodingAPIResponse(**data) for data in result]

    async def reverse(self, lat: float, lon: float, limit: int = None) -> List[models.GeocodingAPIResponse]:
        params: Dict[str, Any] = {
            "lat": lat,
            "lon": lon,
            "appid": self.appid,
        }
        if limit:
            params.update({"limit": limit})
        result = await self._json_request(url="/reverse", params=params)
        return [models.GeocodingAPIResponse(**data) for data in result]


async def icon(icon_id: str) -> bytes:
    client = OpenWeatherBase(appid="")
    client.base_url = "http://openweathermap.org/img/wn"
    url = f"/{icon_id}@2x.png"
    binary = await client._binary_request(url=url)
    return binary


async def weather_map(layer: str, lat: float, lon: float, zoom: int):
    url = "/weathermap"
    params = {"basemap": "map", "cities": "false", "layer": layer, "lat": lat, "lon": lon, "zoom": zoom}
    client = OpenWeatherBase(appid="")
    client.base_url = "https://openweathermap.org"
    result = await client._binary_request(url=url, params=params)
    return result.decode("utf-8")
